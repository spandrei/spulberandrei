
const FIRST_NAME = "Spulber";
const LAST_NAME = "Andrei";
const GRUPA = "1083";

function initCaching() {
   var object = {about:0,home:0,contact:0};
   object.pageAccessCounter = function(n){

       if (n==undefined){
        object.home++; }      
        else{       
       if (n.toUpperCase()=='ABOUT'){
           object.about++;}

       if (n.toUpperCase()=='CONTACT'){
        object.contact++; }

        }
   }

   object.getCache = function(){
       return object;
   }
   return object;}


module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}

